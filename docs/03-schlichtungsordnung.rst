Schlichtungsordnung des L13 e.V.
================================

Beschluss der Mitgliederversammlung am 16.07.2018.

§1 Ablauf der Schlichtung
----------------------

1. Jedes Vereinsmitglied kann Anliegen einbringen, wo andere im Verein
   gegen Regeln oder Prinzipien des Vereins verstoßen, insbesondere wo
   es ein gutes Miteinander nicht gegeben sieht.
2. Den so beklagten Menschen oder Organisationen wird die Angelegenheit
   mitgeteilt und Gelegenheit zu einer Stellungnahme gegeben.
3. Beide Parteien (der Mensch mit dem Anliegen und die Beklagten)
   einigen sich auf eine Schlichtungsperson.
4. Kommt in angemessener Zeit keine Einigung zustande, wird die
   Schlichtungsperson von der Kerngruppe bestimmt.
5. Die Schlichtungsperson kommuniziert mit den Parteien, macht sich ein
   Bild und macht einen allparteilichen Vorschlag zu der Angelegenheit.
6. Der Vorschlag kann sich an die Parteien, Kerngruppe,
   Mitgliederversammlung und die Mitglieder richten.
7. Wird der Vorschlag von den Konfliktparteien nicht angenommen, müssen
   sich Kerngruppe und Mitgliederversammlung damit befassen.

§2 Weitere Regelungen
------------------

1. Ein Anliegen kann auch anonym eingebracht werden.
2. Es wird dann weiter verfolgt, wenn und sobald sich ein Mitglied
   dieses Anliegen zu eigen macht.
3. Wenn eine Partei Gruppe oder Organisation ist, kann die
   Schlichtungsperson die Nennung einer Ansprechperson in dieser
   Angelegenheit verlangen und die Partei muss dem nachkommen.
4. Von den Parteien und der Schlichtungsperson kann verlangt werden,
   ihre Äußerungen schriftlich in ein geeignetes Onlinesystem zu geben.
5. Alle schriftlichen Äußerungen und der Vorgehensvorschlag sind der
   Schlichtungsperson und den Parteien zugänglich. Sie sind
   grundsätzlich auch allen Vereinsmitgliedern zugänglich, wenn und
   insofern nicht der Schutz von Persönlichkeitsrechten überwiegt. Die
   Informationen sind nicht für Außenstehende bestimmt und alle
   Vereinsmitglieder sind verpflichtet, sie streng vertraulich zu
   behandeln.
6. Die Schlichtungsperson berichtet der Mitgliederversammlung und wird
   von ihr entlastet.

