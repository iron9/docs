Satzung des L13 e.V.
====================

Beschluss der Mitgliederversammlung am 16.07.2018.

§1 Name, Sitz, Eintragung, Geschäftsjahr
-------------------------------------

Der Verein heißt *Verein zur Förderung der Versorgung mit nachhaltig
erzeugten Lebensmitteln L13*, in Kurzform *L13*, hat seinen Sitz in
Freiburg, und soll ins Vereinsregister eingetragen werden.

Geschäftsjahr ist das Kalenderjahr.

§2 Zweck, Umsetzung, Soziale Nachhaltigkeit
----------------

Der Verein ist ein Mittel, unsere gemeinsamen Ziele zu erreichen. Wir
drücken das auch dadurch aus, dass wir hier an mehreren Stellen statt
*Der Verein* einfach *wir* schreiben.

1. Zwecke des Vereins sind

   1. die Förderung des Natur- und Umweltschutzes,
   2. der Landschaftspflege und des Tierschutzes
   3. sowie der Verbraucheraufklärung,
   4. insbesondere der Förderung des regionalen ökologischen Land- und
      Gartenbaus.

2. Die Förderung soll mittelbar durch die Entwicklung der notwendigen
   sozialen Strukturen und Vermarktungsformen erfolgen.
3. Die Satzungszwecke sollen verwirklicht werden durch:

   1. Verteilung umweltgerecht erzeugter Produkte; Durch die Verteilung
      umweltgerecht erzeugter Produkte wird keine Gewinnerzielung
      angestrebt.
   2. Vermeidung unnötigen Mülls durch Reduzierung der Zwischenhändler
      und eigenständiges Abfüllen der Produkte von jedem einzelnen
      Mitglied
   3. Förderung der ErzeugerInnen von umweltgerecht und regional
      erzeugten Produkten; Zur Förderung in diesem Sinne gehört der
      Ankauf dieser Produkte durch den Verein und der nicht Gewinn
      bringende Verkauf an Vereinsmitglieder.
   4. Herstellung eines ständigen Kontaktes zwischen VerbraucherInnen
      und ErzeugerInnen, der eine Entflechtung und Transparenz des
      Marktes ermöglicht, den ProduzentInnen den Absatz eines Teils
      ihrer Produkte sichert und den VerbraucherInnen Einfluss auf das
      Produktionsgeschehen lässt.

4. Dabei legen wir Wert auf ein gutes Miteinander. Wir wertschätzen dass
   jedermensch eigene Geschichten, Vorerfahrungen und Sichtweisen
   mitbringt und wollen diese Vielfalt für die gemeinsame Sache nutzen.
5. Wir achten gemeinsam darauf, dass wir auch innerhalb des Vereins sozial
   nachhaltig und ohne Selbstausbeutung arbeiten.
   Sollten sich einmal  Engpässe bei der Besetzung der Kerngruppen-Aufgaben ergeben, 
   wird per *solidarischem Bestellstop* die Geschäftstätigkeit eingestellt, 
   bis der Engpass behoben ist.


§3 Nonprofit-Prinzip
-----------------

1. Der Verein ist selbstlos tätig und verfolgt nicht in erster Linie
   eigenwirtschaftliche Zwecke.
2. Vereinsmittel dürfen nur für satzungsgemäße Zwecke verwendet werden.
   Niemand darf durch zweckfremde Ausgaben oder überhöhte Vergütungen
   begünstigt werden.
3. Wir sehen uns als gemeinnützig, streben aber vor einer grundlegenden
   Änderung der Abgabenordnung nicht die Anerkennung beim Finanzamt an.

§4 Konsent und Konsens
-------------------

1. Alle Entscheidungen erfolgen grundsätzlich nach den Prinzipien des
   Konsent im Sinne der Holakratie (Holacracy) Grundordnung in der
   Übersetzung von CidPartners, aktuell:
   (https://cidpartners.de/wp-content/uploads/2015/12/20151004_Holacracy_Konstitution_Deutsch_4_1.pdf).
   Sie ist dieser Satzung beigefügt, aber nicht ihr Bestandteil.
2. Auch die weiteren Holakratie Gestaltungsprinzipien werden auf unsere
   Vereinsarbeit angepasst, also insbesondere

   1. Dynamische ("agile") Steuerung
   2. (Konsent-)Kreise (Zirkel, z.B. MV, Vorstand, Themengruppen) und
   3. Rollen mit
   4. Verantwortlichkeiten (Accountabilities) und
   5. Zuständigkeiten (Domains),
   6. Eigenverantwortliches Handeln (Individual Action)
   7. sowie Transparenzpflichten.

3. Wo abweichend davon Konsens-Prinzip vorgesehen ist (z.B.
   Vereinszweck), bedeutet das eine einstimmige Entscheidung ohne
   Gegenstimme.
4. Die Vereinsordnung kann Anpassungen und Detailregelungen vorsehen.

§5 Kommunikation, Beschlüsse, Protokolle
-------------------------------------

1. Alle Kommunikation soll offen und transparent erfolgen, so dass auch
   Menschen beteiligt werden, die nicht bei den Treffen sind.
2. Wenn in dieser Satzung von schriftlich gesprochen wird, dann meint
   das auch elektronische Kommunikation und E-Mail (also laut Gesetz
   Textform nicht Schriftform).
3. Änderungen von Satzung, Vereinsordnung, Schlichtungsordnung müssen
   schriftlich erfasst und von der Sitzungsleitung und dem
   protokollführenden Menschen unterzeichnet werden.
4. Alle anderen Beschlüssen können per Mail oder (vorzugsweise) in einem
   gemeinsamen Onlinesystem erfolgen.
5. Weiteres kann die Vereinsordnung regeln.
6. Für etwaige Unstimmigkeiten bei Beschlüssen, Protokollen und
   Umsetzung gilt die Schlichtungsordnung.

§6 (Ordentliche) Mitglieder
------------------------

1. Mitglied kann jeder Mensch und jede Organisation werden, der/die die
   Ziele des Vereins unterstützt.
2. Über den Antrag auf Aufnahme in den Verein entscheidet die Kerngruppe.
3. Die Mitgliedschaft endet durch Austritt, Ausschluss oder Tod.
4. Die Mitgliedschaft ist nicht übertragbar und nicht vererbbar.
5. Der Austritt erfolgt durch schriftliche Erklärung gegenüber dem
   Vorstand.
6. Es wird ein Mitgliedsbeitrag und eine Einlage erhoben. Art und Höhe
   und Zahlungsweise von Mitgliedsbeiträgen und Einlagen,
   Aufnahmeverfahren und Höchstmitgliederzahl, Zugangssperren zu den
   Verteilräumen, Ausschluss, Austritt, verpflichtende Nutzung von
   (Online)-Formularen und Weiteres regelt die Vereinsordnung.

§7 Gastmitglieder
--------------

1. Gastmitglied kann jeder Mensch werden, der/die die Ziele des Vereins
   unterstützt.
2. Auf Mitgliederversammlungen besitzen die Gastmitglieder Rede- und
   Antragsrecht, aber kein Stimmrecht.
3. Art und Höhe von Mitgliedsbeiträgen und Weiteres regelt die
   Vereinsordnung

§8 Fördermitglieder
----------------

1. Fördermitglied kann jeder Mensch und jede Organisation werden,
   der/die die Ziele des Vereins unterstützt.
2. Auf Mitgliederversammlungen besitzen die Fördermitglieder Rede- und
   Antragsrecht, aber kein Stimmrecht.
3. Art und Höhe von Mitgliedsbeiträgen und Weiteres regelt die
   Vereinsordnung

§9 Struktur des Vereins
--------------------

1. Die Vereinsarbeit gliedert sich wie folgt:

   1. Die Mitgliederversammlung ist das oberste Vereinsorgan
   2. Die Kerngruppe besteht aus den Vorstandsmitgliedern und weiteren
      tragenden Mitgliedern und koordiniert und beschließt zwischen den
      Mitgliederversammlungen als erweiterter Vorstand die
      Vereinsangelegenheiten
   3. Der Vorstand vertritt den Verein nach außen.
   4. Themengruppen / Themenbeauftragte arbeiten zu festgelegten Themen.
   5. Kassenprüfungsbeauftragte prüfen die ordnungsgemäße Verwendung und
      Verbuchung der Finanzen.

§10 Mitgliederversammlung
---------------------

1.  Die Mitgliederversammlung ist das oberste Vereinsorgan.
2.  Als Konsent-Kreis ist sie der oberste Kreis. Moderation
    (Facilitation) und Schriftführung werden von der Kerngruppe benannt.
3.  Eine Mitgliederversammlung wird mindestens einmal jährlich von der
    Kerngruppe einberufen, möglichst im ersten Kalender-Halbjahr.
4.  Die Einladung zur Mitgliederversammlung muss die Tagesordnung
    enthalten und jedem Mitglied spätestens am 13. Tag vor der
    Versammlung per E-Mail (an die letzte mitgeteilte Adresse)
    zugeschickt werden.
5.  Die Mitgliederversammlung ist beschlussfähig, wenn erfüllt ist, dass
    mindestens 7 Mitglieder und mindestens 10 % der Mitglieder anwesend
    sind. Sollte keine Beschlussfähigkeit vorhanden sein, so ist die
    Mitgliederversammlung bei zweiter Einladung ohne Rücksicht auf die
    Zahl der erschienenen Mitglieder beschlussfähig, sofern das in der
    Einladung ausdrücklich angekündigt wurde. Ebenso ist die
    Mitgliederversammlung bei einer Mitgliederanzahl von unter 7
    Mitgliedern ohne Rücksicht auf die Zahl der erschienen Mitglieder
    beschlussfähig.
6.  Die Mitgliederversammlung

    1. entscheidet über Änderungen von Satzung, Satzungszweck,
       Vereinsordnung, Schlichtungsordnung
    2. entscheidet über alle Fragen der Gesamtausrichtung des Vereins
    3. ist zuständig für Wahl, Abwahl und Zuwahl der Kerngruppe
       (Vorstand und weitere tragende Mitglieder) und der
       Kassenprüfungsbeauftragten
    4. nimmt die Berichte des Vorstands entgegen und entscheidet über
       die Entlastung (nachträgliche Gutheißung der Arbeit)
    5. beschließt die Auflösung des Vereins und die Verwendung des (nach
       anteiliger Auszahlung der Mitgliedschaftsanteile) ggf.
       verbleibenden Restvermögens
    6. entscheidet über Aufnahme und Ausschluss von Mitgliedern in
       Berufungsfällen
    7. nimmt weitere Aufgaben wahr, soweit sie sich aus der Satzung, der
       Vereinsordnung oder nach dem Gesetz ergeben

7.  Die Mitgliederversammlung entscheidet grundsätzlich nach dem
    Konsent-Verfahren gem. §4 dieser Satzung.
8.  Ausnahme: Einvernehmlich ohne Gegenstimme (Konsens) wird entschieden

    1. über eine Änderung des Satzungszwecks oder die Auflösung des
       Vereins.
    2. jede Angelegenheit, falls diese Änderung des Beschlussverfahrens
       in derselben Versammlung einvernehmlich ohne Gegenstimme
       beschlossen wird.

9.  Kommt es in Bezug auf einen Antrag zur Änderung des Satzungszwecks
    oder Auflösung des Vereins in einer Mitgliederversammlungen nicht
    zum einvernehmlichen Beschluss (Konsens), dann kann zu dieser Frage
    in der folgenden Mitgliederversammlung - wenn das ausdrücklich in
    der Einladung angekündigt wird - per Abstimmung mit 3/4 Mehrheit
    entschieden werden.
10. Über die Ergebnisse der Mitgliederversammlung wird ein Protokoll
    angefertigt, das vom protokollierenden Menschen und der
    Sitzungsleitung unterzeichnet wird.
11. Wo die Satzung Mehrheitsentscheidungen vorsieht, gilt: Jedes
    Mitglied hat eine Stimme. Das Stimmrecht kann nur persönlich oder
    für ein Mitglied unter Vorlage einer schriftlichen Vollmacht
    ausgeübt werden.
12. Der Vorstand kann außerordentliche Mitgliederversammlungen
    einberufen und ist dazu verpflichtet, wenn mindestens ein Viertel
    der Mitglieder dies schriftlich unter der Angabe von Gründen
    verlangt. Eine Einberufung durch den Vorstand muss dann innerhalb
    einer Frist von drei Monaten erfolgen.
13. Weiteres regelt die Vereinsordnung.

§11 Kerngruppe (erweiterter Vorstand)
---------------------------------

1.  Die Kerngruppe ist für alle Fragen zuständig, die nicht per Satzung
    oder Beschluss anders zugewiesen sind. Sie erarbeitet sich dazu
    fortlaufend eine *gemeinsame Vorstellung vom großen Ganzen*, die
    schriftlich in Konzepten und darüber hinaus im lebendigen
    Diskussionsprozess lebt. Sie führt die Geschäfte des Vereins nach
    den Vorgaben der Mitgliederversammlung. Grundlegende Fragen zur
    Gesamtausrichtung des Vereins legt sie der Mitgliederversammlung zum
    Beschluss vor.
2.  Zusätzlich darf sie die Satzung ändern, insofern sie vom
    Registergericht oder dem Finanzamt beanstandet wird.
3.  Die Kerngruppe besteht aus den *Vorstandsmitgliedern* und *weiteren
    tragenden Mitgliedern*. Jedes Kerngruppenmitglied hat dabei
    festgelegte Zuständigkeiten.Als Konsent-Kreis wird die Kerngruppe
    von der Mitgliederversammlung beauftragt. Die Vorstandsmitglieder
    übernehmen gemeinschaftlich die Rolle des Lead Links.
4.  Entscheidungen werden im Konsent gefällt.
5.  Sie macht ihre Arbeit und insbesondere Entscheidungen fortlaufend
    per Rundmail und/oder Onlineportal transparent.
6.  Ihre beschließenden Sitzungen werden vereinsintern bekannt gemacht
    und stehen allen Mitgliedern zur beratenden Teilnahme offen. Davon
    kann aus wichtigem Grund (z.B. Vertraulichkeitserfordernisse)
    abgewichen werden.
7.  Jedes Mitglied der Kerngruppe hält selbst aktiv den
    Informationsfluss aufrecht und steht auch zwischen den Sitzungen für
    informelle Fragen von Mitgliedern, Vorständen und anderen Gruppen
    bereit.
8.  Jedes Mitglied der Kerngruppe gibt frühzeitig Meldung, wenn es dem
    Kerngruppen-Diskussionsprozess („gemeinsame Vorstellung vom großen
    Ganzen“) nicht mehr aktiv folgen, oder andere vereinbarte Aufgaben
    und Zuständigkeiten nicht mehr abdecken kann.
9.  Ihre Mitglieder unterstützen sich gegenseitig bei ihrer gemeinsamen
    (gesamtschuldneri­schen) Verantwortlichkeit. Dazu hält jedes
    Mitglied die Gruppe aktiv darüber auf Stand, wie viel Zeit und
    Energie es kurz- und mittelfristig in die Kerngruppenarbeit stecken
    kann.
10. Alle Mitglieder der Kerngruppe üben ihre Tätigkeit ehrenamtlich aus,
    können jedoch für ihre Tätigkeit eine angemessene
    Vergütung/Aufwandsentschädigung erhalten. Über die Art und Höhe
    entscheidet die Mitgliederversammlung. Dabei sollte §31 BGB mit
    seiner erhöhten Haftung bei Überschreiten der Ehrenamtspauschale
    beachtet werden.
11. Die Kerngruppe führt fortlaufend ein Arbeitskonzept (und legt es
    anlässlich der Wahlen der Mitgliederversammlung vor), in dem sie
    insbesondere regelt und transparent macht,

    1. welche Zuständigkeiten bei einer Wahl mindestens abgedeckt werden
       müssen.
    2. in welche Arbeitsbereiche (initial: Abläufe, Finanzen, Team) die
       Arbeit des Vereins aufgeteilt wird und wie die Fachgruppen und
       Fachbeauftragten diesen zugeordnet werden.
    3. wie sie ihre Sitzungsfolge gestaltet.
    4. optional: wie elektronische Beschlussfassungen erfolgen.

12. Weiteres regelt die Vereinsordnung.

§12 Wahl von Vorständen und weiteren tragenden Mitgliedern
------------------------------------------------------

1. Kerngruppenmitglieder (Vorstandsmitglieder und weitere tragende
   Mitglieder) werden von der Mitgliederversammlung gewählt. Für jedes
   Mitglied wird dabei Status (Vorstand oder nicht) und Zuständigkeiten
   festgelegt. Die Amtszeit beträgt regulär 6 Monate.
2. Kerngruppenmitglieder können nur Mitglieder des Vereins werden.
3. Bei Beendigung der Mitgliedschaft im Verein endet auch das Amt als
   Kerngruppenmitglied.
4. Nach Ende der Amtszeit bleiben die Kerngruppenmitglieder bis zu einer
   Neuwahl im Amt.
5. Eine vorzeitige Neuwahl oder Zuwahl durch die Mitgliederversammlung
   ist möglich.
6. Blockwahl ist zulässig. Wiederwahl ist zulässig.
7. Weitere tragende Mitglieder können auch durch Beschluss der
   Kerngruppe zugewählt, abgewählt, oder beurlaubt werden. Ebenso können
   die Zuständigkeiten aller Kerngruppenmitglieder von der Kerngruppe
   neu beschlossen werden. Vereinsmitglieder werden über
   Kerngruppensitzungen in denen Änderungen der Kerngruppenmitglieder
   beschlossen werden per Mail informiert (Einladung und Protokoll).
8. Weiteres regelt die Vereinsordnung.

§13 Vorstand
--------

1. Der Vorstand besteht aus 6 natürlichen Personen, von denen 3 als
   *Senioren* schon Vorerfahrung haben sollen, und 3
   als *Junioren* weniger Erfahrung haben sollen. Die
   Mitgliederversammlung kann bei der Wahl für die jeweilige Amtszeit
   begründet von dieser Zahl und Zusammensetzung abweichen. Dabei muss
   die Anzahl der Vorstände mindestens 3 betragen.
2. Er ist Vorstand im Sinne des BGB und vertritt den Verein damit nach
   außen (gerichtlich und außergerichtlich).
3. Jedes Vorstandsmitglied ist einzeln vertretungsberechtigt.
4. Weiteres regelt die Vereinsordnung.

§14 Fachgruppen und Fachbeauftragte
-------------------------------

1. Fachgruppen arbeiten jeweils im Rahmen einer von der Kerngruppe
   festgelegten fachlichen Zuständigkeit. Jede Fachgruppe ist einem
   Arbeitsbereich zugeordnet.
2. Als Konsent-Kreise werden sie von der Kerngruppe oder ihrem
   jeweiligen Arbeitsbereich eingesetzt, beauftragt, koordiniert und
   aufgelöst. (Die Arbeitsbereiche sind damit Konsent-Kreise, die sich
   in der Regel mit direkter Kommunikation statt Sitzungen
   koordinieren.)
3. Sie machen ihre Arbeit fortlaufend per Rundmail und/oder Onlineportal
   transparent.
4. Die *Fachgruppen-Koordinierenden* (Holakratie: Lead Links) werden von
   der Kerngruppe auf Vorschlag der Fachgruppe eingesetzt. Sie vertreten
   die Sicht der jeweiligen Bereichskoordinierenden in der Fachgruppe
   und stehen dafür ein, dass die jeweilige Fachgruppe ihren Auftrag
   erfüllt. Sie führen dazu insbesondere fortlaufend ein Konzept der
   Arbeit ihrer Fachgruppe.
5. Die *Fachgruppen-Sprechenden* (Holakratie: Rep Links) werden von
   der Fachgruppe bei Bedarf selbst gewählt. Sie vertreten die Sicht der
   Fachgruppe gegenüber den jeweiligen Bereichskoordinierenden und
   stehen dafür ein, dass die Fachgruppe zur Erfüllung ihres Auftrags
   alles hat, was sie benötigt.
6. Wurde kein Fachgruppen-Sprechendes gewählt, vertritt das jeweilige
   Fachgruppen-Koordinierende diese Aufgabe. Es kann jederzeit die Wahl
   eines Fachgruppen-Sprechers verlangen.
7. *Fachbeauftragte* (Holakratie: Rollen) werden dort eingesetzt, wo
   eine fachspezifische Aufgabe von einer Einzelperson bearbeitet wird.
   Die Kerngruppe kann jederzeit Fachbeauftragte in Fachgruppen
   umwandeln, und umgekehrt. Jedes Fachbeauftragte ist einem
   Arbeitsbereich zugeordnet.
8. Weiteres regelt die Vereinsordnung.

§15 Kassenprüfungsbeauftragte
-------------------------

1. Die Mitgliederversammlung wählt gemeinsam mit dem Vorstand und für
   die gleiche Amtszeit ein oder mehrere Kassenprüfungsbeauftragte.
2. Sie prüfen möglichst fortlaufend, jedoch spätestens vor der
   Jahres-Mitgliederversammlung, dass die Vereinsmittel ordnungsgemäß
   (in Bezug auf Satzung, Vereinsordnung und Beschlüsse) verwendet und
   verbucht werden, und erstatten der Mitgliederversammlung Bericht.
3. Haben dabei verschiedene Kassenprüfungsbeauftragte unterschiedliche
   Sichtweisen, werden diese der Mitgliederversammlung vollständig
   berichtet.
4. Die Kassenprüfungsbeauftragten dürfen nicht Mitglied des Vorstands
   sein.
5. Wiederwahl ist zulässig.

§16 Frage- und Antragsrechte
------------------------

1. Jedes Mitglied kann an Vorstand, Themengruppen und Themenbeauftragte
   Fragen formulieren oder Anträge stellen.
2. Solche Fragen oder Anträge müssen vom Adressaten baldmöglichst
   behandelt und entschieden bzw. begründet beantwortet werden.
3. Unstimmigkeiten, was solche Entscheidungen oder Antworten betrifft,
   können in einem Schlichtungsverfahren behandelt werden.
4. Das gleiche gilt, wenn das Frage- und Antragsrecht missbräuchlich
   genutzt wird.
5. Weiteres kann die Vereinsordnung regeln. Sie kann insbesondere die
   Nutzung eines Online-Systems vorschreiben und ein anonymes Frage- und
   Antragsrecht vorsehen.

§17 Vereinsordnung
--------------

1. Die Vereinsordnung regelt Details, Konkretisierungen, und - wo die
   Satzung es vorsieht - Ausnahmen dieser Satzungsregelungen und ist
   verbindlich für alle Mitglieder des Vereins..
2. Die Vereinsordnung ist nicht Bestandteil der Satzung. Sie muss damit
   im Gegensatz zur Satzung nicht bei jeder Anpassung notariell ins
   Vereinsregister eingetragen werden und soll fortlaufend an die
   aktuellen Erfordernisse angepasst werden.
3. Für Änderungen der Vereinsordnung gelten die gleichen Regelungen wie
   für Änderungen der Satzung selbst.

§18 Schlichtungsordnung
-------------------

1. Zur Regelung eventueller Konflikte geben wir uns eine
   Schlichtungsordnung, die für diesen Fall auch Rechte und Pflichten
   von Mitgliedern regelt.
2. Die Schlichtungsordnung ist nicht Bestandteil der Satzung.
3. Für Änderungen der Schlichtungsordnung gelten die gleichen Regelungen
   wie für Änderungen der Satzung selbst.

§19 Haftung (Gesetzesauszug)
------------------------

1. Der Verein ist für den Schaden verantwortlich, den der Vorstand, ein
   Mitglied des Vorstandes oder ein anderer verfassungsmäßig berufener
   Vertreter durch eine in Ausführung der ihm zustehenden Verrichtungen
   begangene, zum Schadensersatz verpflichtende Handlung einem Dritten
   zufügt (§ 31 BGB).
2. Vorstände und Mitglieder haften nur bei Vorsatz und großer
   Fahrlässigkeit, wenn sie ehrenamtlich tätig sind (also nicht mehr als
   die Ehrenamtspauschale von aktuell 720€/Jahr bekommen), sonst auch bei
   einfacher Fahrlässigkeit (§31a,b BGB).
3. Für Verbindlichkeiten des Vereins haftet nur das Vereinsvermögen.

§20 Datenschutz
-----------

1. Wir verarbeiten Daten von Mitgliedern und Nichtmitgliedern
   (Geschäftspartnern, Interessierten).
2. Dabei achten wir auf den (auch gesetzlich vorgeschriebenen)
   Datenschutz und beachten insbesondere Vertraulichkeit und
   Zweckbindung.
3. Um gut miteinander arbeiten zu können, stellen wir Teile der
   Kontaktdaten (Name, E-Mail-Adresse, Telefonnummer) intern allen
   Mitgliedern zur Verfügung.
4. Weiteres regelt die Vereinsordnung.

§21 Heilungsklausel
---------------

1. Sollten Bestimmungen dieser Satzung oder eine künftig in ihn
   aufgenommenen Bestimmung ganz oder teilweise rechtsunwirksam (z.B.
   nach §139 BGB) oder nicht durchführbar sein, so soll hierdurch die
   Gültigkeit der übrigen Bestimmungen der Satzung nicht berührt werden.
2. Das gleiche gilt, soweit sich herausstellen sollte, dass die Satzung
   eine Regelungslücke enthält.
3. Anstelle der unwirksamen oder undurchführbaren Bestimmung oder zur
   Ausfüllung der Lücke soll eine angemessene Regelung gelten, die,
   soweit rechtlich möglich, dem am nächsten kommt, was der Verein
   gewollt hat oder nach dem Sinn und Zweck der Satzung gewollt hätte,
   sofern sie bei Abschluss der Satzung oder bei der späteren Aufnahme
   einer Bestimmung den Punkt bedacht hätte.

