Vereinsordnung des L13 e.V.
===========================

Beschluss der Mitgliederversammlung am 16.07.2018.

§1 Konsent-Prinzip und Holacracy
-----------------------------

1. Der Verein arbeitet grundsätzlich nach der Holacracy-Konstitution 4.1
   in der Übersetzung von Cid-Partners.
   https://cidpartners.de/wp-content/uploads/2015/12/20151004_Holacracy_Konstitution_Deutsch_4_1.pdf
2. Anpassungen und Details werden in dieser Vereinsordnung geregelt und
   fortlaufend angepasst.

§2 Mitgliedsbeitrag & -einlage
---------------------------

§3 Aufnahme von Mitgliedern
1. Mitgliedsbeitrag

   1. Pro Person fällt ein monatlicher Mitgliedsbeitrag an, dieser wird
      vom Mitglied selbst festgelegt, als Richtwert werden 5€ pro Monat
      empfohlen.
   2. Nutzt ein Mitglied die Infrastruktur des Vereins für weitere
      Personen, ist der empfohlene Richtwert entsprechend höher.

2. Start-Regelung

   1. Die Kerngruppe definiert einen Starttermin.
   2. Mitglieder, die vor diesem Termin Mitglied geworden sind, brauchen
      für das Jahr 2018 keine Beiträge zahlen, stattdessen geben sie dem
      Verein einen freiwilligen Kredit, dessen Richtwert sich aus der
      Höhe des monatlichen Beitragsrichtwertes multipliziert mit 12
      ergibt.
   3. Der Kredit wird bei Austritt der Mitglieder und entsprechender
      Liquidität zurückgezahlt.

3. Mitgliedseinlage

   1. Pro Person fällt eine einmalige Einlage an, diese wird vom
      Mitglied selbst festgelegt, als Richtwert werden 100€ empfohlen.
   2. Nutzt ein Mitglied die Infrastruktur des Vereins für weitere
      Personen, ist der empfohlene Richtwert entsprechend höher.

4. Gastmitglieder

   1. Jedes Mitglied kann Gastmitglieder in den Verein aufnehmen.
   2. Gastmitglieder unterschieden sich zu Mitgliedern in dem Sinne,
      dass sie selbst keinen Beitrag und keine Einlage zahlen, sie kein
      Stimmrecht haben und dass von ihnen keine aktive Mitarbeit im
      Verein erwartet wird.
   3. Gastmitglieder sind einem ordentlichen Mitglied zugeordnet,
      welches für sie bürgt und für sich nach eigenem Ermessen höhere
      Beiträge und Einlagen zahlt.

5. Gemeinsame Abrechnung

   1. Mitglieder können ihre finanziellen Pflichten gegenüber dem Verein
      über eine Person abrechnen lassen.
   2. Dazu gibt ein Mitglied an, welcher Mitglieder Verpflichtungen über
      dessen Mitgliedschaft abgerechnet werden sollen und ist verpflichtet
      Mitgliedsbeitrag, -einlage und andere Verpflichtungen für alle
      genannten Mitglieder zu begleichen.

------------------------

1. Über die Aufnahme von Mitgliedern entscheidet die Kerngruppe. Sie
   kann diese Aufgabe delegieren.
2. Gegen die Ablehnung eines Aufnahmeantrags kann mit einer Frist von
   vier Wochen schriftlich Einspruch erhoben werden. In diesem Fall wird
   ein Schlichtungsverfahren durchgeführt, danach entscheidet auf Antrag
   der abgelehnten Partei die Mitgliederversammlung endgültig.
3. Von ordentlichen Mitgliedern vorgeschlagene Gastmitglieder können per
   Beschluss durch ein automatisiertes Verfahren aufgenommen werden.
4. Noch zu regeln: Aufnahmegrenze

§4 Ausscheiden von Mitgliedern
---------------------------

1. Ausscheidende Vereinsmitglieder erhalten ihre Einlagen zurück.
2. Sollten sie mit ihren Beitragszahlungen im Rückstand sein, werden
   diese von der Einlage abgezogen, und sie erhalten nur den Restbetrag
   zurück.
3. Darüber hinaus müssen sonstige ausstehende Beträge vor dem Austritt
   beglichen werden.
4. Die Rückzahlung der Einlagen kann nicht verlangt werden, solange der
   Verein dieses Kapital zur Erfüllung seiner fälligen Verbindlichkeiten
   benötigt.
5. Auf Rücklagen oder sonstiges Vermögen besteht kein Anspruch.
6. Im Falle der Zahlungsunfähigkeit des Vereins werden keine Einlagen
   ausgezahlt.

§5 Ausschluss von Mitgliedern
--------------------------

1. Ein Mitglied kann ausgeschlossen werden, wenn die vereinbarte
   Beitragszahlung nicht innerhalb einer Frist von drei Monaten
   geleistet wird oder grob vereinsschädigendes Verhalten vorliegt.
2. Gegen den Ausschluss kann mit einer Frist von vier Wochen schriftlich
   Einspruch erhoben werden.
3. In diesem Fall wird ein Schlichtungsverfahren durchgeführt, danach
   entscheidet auf Antrag des ausgeschlossenen Mitglieds die
   Mitgliederversammlung endgültig.
4. Bis zu einer Entscheidung ruht die Mitgliedschaft.

§6 Mitgliederversammlung
---------------------

1. Rechtliche Klarstellung: Es ist nicht möglich, wirksam über Dinge zu
   beschließen, die sich nicht aus der versendeten Tagesordnung klar
   ergeben.
2. Bei Beschlüssen über die Satzung oder andere Vereinsdokumente muss
   ein Entwurf mit Erläuterung der Änderungen mit der Einladung
   versendet werden.
3. Die Sitzungsleitung wird vom Vorstand durchgeführt oder delegiert.
4. Zu Beginn der Mitgliederversammlung ist ein Protokollmensch zu
   wählen.

§7 Datenschutz
-----------

1. Von den Mitgliedern werden Kontaktdaten (Name, Anschrift, E-Mail,
   Telefonnummer) und Zahlungsdaten (Bankverbindung) abgefragt und
   verarbeitet.
2. Weitere Informationen (auch von Nichtmitgliedern) werden
   grundsätzlich nur verarbeitet, wenn sie zur Förderung des
   Vereinszweckes nützlich sind und keine Anhaltspunkte bestehen, dass
   die betroffene Person ein schutzwürdiges Interesse hat, das der
   Verarbeitung entgegensteht.
3. All diese Informationen werden dabei durch geeignete technische und
   organisatorische Maßnahmen vor der Kenntnisnahme Dritter geschützt.
4. Alle Mitglieder müssen ihnen zugängliche Informationen entsprechend
   vertraulich behandeln und sie Dritten nicht zugänglich machen.
5. Mit Dritten, die diese Informationen verarbeiten, müssen
   entsprechende Verträge geschlossen werden.
6. Wenn ein Mitglied austritt, werden personenbezogenen Daten gelöscht.
   Bestehen bleiben die Daten der Online-Zusammenarbeit und
   aufbewahrungspflichtige Daten (z.B. Buchhaltung).

